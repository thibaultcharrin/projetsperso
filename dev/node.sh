#!/bin/bash
function linuxb_node() {
    echo -e "\n${ROCKET} Node (via nvm - npm: force install if shell changed) "
    VERSION=0.40.1 # CHANGE ME :)

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Node (via nvm - npm: force install if shell changed) "
        echo "https://docs.npmjs.com/downloading-and-installing-node-js-and-npm"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            echo "macOS is not yet supported"
            return 1
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v npm >/dev/null ||
            ! command -v node >/dev/null; then
            # Before_Script
            # curl
            if ! command -v curl >/dev/null; then
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} curl
            fi

            # Script
            curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v"${VERSION}"/install.sh | bash

            export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
            [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

            command -v nvm

            nvm install node # "node" is an alias for the latest version

            npm -v
            node -v
        else
            echo "Already present "
            npm -v
            node -v
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
CURRENT_PATH=$(pwd)
cd ../resources || exit 1
# shellcheck source=/dev/null
. ./unicode-emojis.sh
# shellcheck source=/dev/null
. ./os-check.sh
cd "$CURRENT_PATH" || exit 1
linuxb_node "$@"

#!/bin/bash
function linuxb_python() {
    echo -e "\n${PYTHON} Python3 (force install if venv is missing) "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Python3 (force install if venv is missing) "
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            echo "macOS is not yet supported"
            return 1
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v python3 >/dev/null; then
            case $(grep "^ID=" /etc/os-release | cut -d "=" -f 2 | tr -d '"') in
            "ubuntu" | "debian")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} python3-full
                ;;
            *)
                echo "Generic installation not yet supported"
                return 1
                ;;
            esac
        else
            echo "Already present "
            python3 --version
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
CURRENT_PATH=$(pwd)
cd ../resources || exit 1
# shellcheck source=/dev/null
. ./unicode-emojis.sh
# shellcheck source=/dev/null
. ./os-check.sh
cd "$CURRENT_PATH" || exit 1
linuxb_python "$@"

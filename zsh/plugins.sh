#!/bin/bash
function linuxb_plugins() {
    echo -e "\n${UNICORN} Zsh - autosuggestions and syntax-highlighting plugins "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Zsh - autosuggestions and syntax-highlighting plugins "
        echo "https://medium.com/@shivam1/make-your-terminal-beautiful-and-fast-with-zsh-shell-and-powerlevel10k-6484461c6efb"
        ;;
    *)
        linuxb_os_check

        # Before_Script
        # zsh
        if ! command -v zsh >/dev/null ||
            [[ ! -d ~/.oh-my-zsh/ ]]; then
            linuxb_zsh
        fi
        # git
        linuxb_os_check
        if ! command -v git >/dev/null; then
            ${LINUXB_PKG_UPDATE}
            ${LINUXB_PKG_INSTALL} git
        fi

        # Script
        if [[ "$(grep -wc "zsh-autosuggestions" ~/.zshrc)" == 0 ]]; then
            git clone https://github.com/zsh-users/zsh-autosuggestions.git "${ZSH_CUSTOM:-${HOME}/.oh-my-zsh/custom}"/plugins/zsh-autosuggestions
        fi
        if [[ "$(grep -wc "zsh-syntax-highlighting" ~/.zshrc)" == 0 ]]; then
            git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "${ZSH_CUSTOM:-${HOME}/.oh-my-zsh/custom}"/plugins/zsh-syntax-highlighting

            if [[ "$(grep -c "# ENABLE_CORRECTION=" ~/.zshrc)" == 1 ]]; then
                sed -i "s|# ENABLE_CORRECTION=\"true\"|ENABLE_CORRECTION=\"true\"|g" ~/.zshrc
            fi

            if [[ "$(grep -cE "^plugins.*zsh-autosuggestions.*zsh-syntax-highlighting" ~/.zshrc)" != 1 ]]; then
                sed -i "s|plugins=(.*)|plugins=(git zsh-autosuggestions zsh-syntax-highlighting)|g" ~/.zshrc
            fi
        else
            echo "Already present "
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_plugins "$@"

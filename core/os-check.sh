#!/bin/bash
function linuxb_os_check() {
    LINUXB_OS=$(uname)
    LINUXB_PKG_MANAGERS=("apt-get" "zypper" "dnf" "urpmi" "slackpkg" "pacman" "apk" "emerge" "xbps-install" "brew")
    LINUXB_PKG_MANAGER=""
    LINUXB_PKG_UPDATE=""
    LINUXB_PKG_INSTALL=""
    LINUXB_PKG_UPGRADE=""
    LINUXB_PKG_AUTOREMOVE=""
    LINUXB_PKG_AUTOCLEAN=""
    LINUXB_PKG_CLEAN=""

    for BIN in "${LINUXB_PKG_MANAGERS[@]}"; do
        if command -v "${BIN}" >/dev/null; then
            LINUXB_PKG_MANAGER="${BIN}"
            break
        fi
    done

    case ${LINUXB_PKG_MANAGER} in
    "apt-get")
        LINUXB_PREFIX="sudo apt-get "
        LINUXB_PKG_UPDATE=${LINUXB_PREFIX}"update -y"
        LINUXB_PKG_INSTALL=${LINUXB_PREFIX}"install -y"
        LINUXB_PKG_UPGRADE=${LINUXB_PREFIX}"upgrade -y"
        LINUXB_PKG_AUTOREMOVE=${LINUXB_PREFIX}"autoremove -y"
        LINUXB_PKG_AUTOCLEAN=${LINUXB_PREFIX}"autoclean -y"
        LINUXB_PKG_CLEAN=${LINUXB_PREFIX}"clean"
        ;;
    "zypper")
        LINUXB_PREFIX="sudo zypper "
        LINUXB_PKG_UPDATE=${LINUXB_PREFIX}"refresh"
        LINUXB_PKG_INSTALL=${LINUXB_PREFIX}"install -y"
        LINUXB_PKG_UPGRADE=${LINUXB_PREFIX}"update -y"
        LINUXB_PKG_CLEAN=${LINUXB_PREFIX}"clean"
        ;;
    "dnf")
        LINUXB_PREFIX="sudo dnf "
        LINUXB_PKG_UPDATE=${LINUXB_PREFIX}"check-update -y"
        LINUXB_PKG_INSTALL=${LINUXB_PREFIX}"install -y"
        LINUXB_PKG_UPGRADE=${LINUXB_PREFIX}"update -y"
        LINUXB_PKG_AUTOREMOVE=${LINUXB_PREFIX}"autoremove -y"
        ;;
    "urpmi")
        LINUXB_PREFIX="sudo urpmi --auto "
        LINUXB_PKG_UPDATE="sudo urpmi.update -a --auto"
        LINUXB_PKG_INSTALL=${LINUXB_PREFIX}""
        LINUXB_PKG_UPGRADE=${LINUXB_PREFIX}"--auto-select"
        LINUXB_PKG_CLEAN=${LINUXB_PREFIX}"--clean"
        ;;
    "slackpkg")
        LINUXB_PREFIX="sudo slackpkg -batch=on -default_answer=y "
        LINUXB_PKG_UPDATE=${LINUXB_PREFIX}"update"
        LINUXB_PKG_INSTALL=${LINUXB_PREFIX}"install"
        LINUXB_PKG_UPGRADE=${LINUXB_PREFIX}"upgrade-all"
        ;;
    "pacman")
        LINUXB_PREFIX="sudo pacman -S --noconfirm "
        LINUXB_PKG_UPDATE=${LINUXB_PREFIX}"-y"
        LINUXB_PKG_INSTALL=${LINUXB_PREFIX}""
        LINUXB_PKG_UPGRADE=${LINUXB_PREFIX}"-u"
        LINUXB_PKG_CLEAN=${LINUXB_PREFIX}"-c"
        ;;
    "apk")
        LINUXB_PREFIX="sudo apk "
        LINUXB_PKG_UPDATE=${LINUXB_PREFIX}"update"
        LINUXB_PKG_INSTALL=${LINUXB_PREFIX}"add"
        LINUXB_PKG_UPGRADE=${LINUXB_PREFIX}"upgrade"
        LINUXB_PKG_CLEAN=${LINUXB_PREFIX}"cache --purge"
        ;;
    "emerge")
        LINUXB_PREFIX="sudo emerge "
        LINUXB_PKG_UPDATE=${LINUXB_PREFIX}"--sync"
        LINUXB_PKG_INSTALL=${LINUXB_PREFIX}""
        LINUXB_PKG_UPGRADE=${LINUXB_PREFIX}"-NuDa world"
        LINUXB_PKG_CLEAN=${LINUXB_PREFIX}"--depclean -c"
        ;;
    "xbps-install")
        LINUXB_PREFIX="sudo xbps-install "
        LINUXB_PKG_UPDATE=${LINUXB_PREFIX}"-Sy"
        LINUXB_PKG_INSTALL=${LINUXB_PREFIX}"-y"
        LINUXB_PKG_UPGRADE=${LINUXB_PREFIX}"-uy"
        LINUXB_PKG_AUTOREMOVE="sudo xbps-remove -ROo"
        ;;
    "brew")
        LINUXB_PREFIX="brew "
        LINUXB_PKG_UPDATE=${LINUXB_PREFIX}"update"
        LINUXB_PKG_INSTALL=${LINUXB_PREFIX}"install"
        LINUXB_PKG_AUTOREMOVE=${LINUXB_PREFIX}"autoremove"
        LINUXB_PKG_CLEAN=${LINUXB_PREFIX}"cleanup --prune=all"
        ;;
    *)
        echo
        echo "Your package manager was not identified: "
        echo "You will need to configure your Linux Distribution manually "
        return 1
        ;;
    esac
    return 0
}
#REGION_REMOVED_BY_BUILD_SH
linuxb_os_check

# Resources
# https://distrowatch.com/dwres.php?resource=package-management

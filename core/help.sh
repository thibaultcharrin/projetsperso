#!/bin/bash
function linuxb_help() {
	echo
	echo "Usage: linuxb [COMMAND] [OPTION]"
	echo
	echo "Bootstrap your Linux distribution with a selection of packages for Development, DevOps and System Administration."
	echo
	echo "Commands:"
	echo "  all                      install all packages"
	echo "  install [PACKAGE]        install specific package, [PACKAGE] is mandatory"
	echo "  list                     list all available packages"
	echo "  upgrade                  upgrade all packages"
	echo "  wsl                      enable systemd for wsl2"
	echo "  zsh                      installs zsh and oh my zsh"
	echo
	echo Global Options:
	echo "  -h, --help           display this help and exit"
	echo "  -v, --version        output version information and exit"
	echo
	echo "Run 'linuxb COMMAND --help' for more information on a command."

	return 0
}
#REGION_REMOVED_BY_BUILD_SH
linuxb_help

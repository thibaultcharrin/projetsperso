#!/bin/bash
function linuxb_misc() {
    echo -e "\n${PENGUIN} Miscellaneous Tools (neofetch/fastfetch jq ranger tree) "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Miscellaneous Tools (neofetch/fastfetch jq ranger tree) "
        echo "https://roadmap.sh/devops"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            echo "macOS is not yet supported"
            return 1
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v jq >/dev/null ||
            ! command -v ranger >/dev/null ||
            ! command -v tree >/dev/null; then
            case $(grep "^ID=" /etc/os-release | cut -d "=" -f 2 | tr -d '"') in
            "ubuntu" | "debian")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} neofetch jq ranger tree
                ;;
            "fedora" | "ol")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} fastfetch jq ranger tree
                ;;
            "arch")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} fastfetch jq ranger tree
                ;;
            "opensuse-tumbleweed")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} fastfetch jq ranger tree
                ;;
            *)
                echo "Generic installation not yet supported"
                return 1
                ;;
            esac
        else
            echo "Already present "
            tree --version
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_misc "$@"

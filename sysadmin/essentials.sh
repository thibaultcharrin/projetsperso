#!/bin/bash
function linuxb_essentials() {
    echo -e "\n${PENGUIN} Essential Tools (vim git curl) "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Essential Tools (vim git curl) "
        echo "https://roadmap.sh/devops"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            ${LINUXB_PKG_INSTALL} vim git curl
            return 0
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v vim >/dev/null ||
            ! command -v git >/dev/null ||
            ! command -v curl >/dev/null; then
            case $(grep "^ID=" /etc/os-release | cut -d "=" -f 2 | tr -d '"') in
            "ubuntu" | "debian")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} vim git curl
                ;;
            "fedora" | "ol")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} vim git curl
                ;;
            "arch")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} vim git curl
                ;;
            "opensuse-tumbleweed")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} vim git curl
                ;;
            *)
                echo "Generic installation not yet supported"
                return 1
                ;;
            esac
        else
            echo "Already present "
            git --version
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_essentials "$@"

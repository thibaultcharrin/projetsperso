#!/bin/bash
function linuxb_knative() {
    echo -e "\n${WAVE} Knative - kn "
    KNATIVE_VERSION=1.16.0 # CHANGE ME :)
    FUNC_VERSION=1.16.1    # CHANGE ME :)

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Knative - kn "
        echo "https://knative.dev/docs/getting-started/quickstart-install/#install-the-knative-cli"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            ${LINUXB_PKG_INSTALL} knative/client/kn
            ${LINUXB_PKG_INSTALL} knative-extensions/kn-plugins/quickstart
            brew tap knative-extensions/kn-plugins
            ${LINUXB_PKG_INSTALL} func
            return 0
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v kn >/dev/null ||
            ! command -v kn-quickstart >/dev/null; then
            # Before_Script
            # curl
            linuxb_os_check
            if ! command -v curl >/dev/null; then
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} curl
            fi
            # Script
            #knative cli
            curl -L -o ~/kn "https://github.com/knative/client/releases/download/knative-v${KNATIVE_VERSION}/kn-linux-amd64"

            chmod +x ~/kn

            sudo mv -v ~/kn /usr/local/bin
            kn version

            #knative quickstart plugin
            curl -L -o ~/kn-quickstart "https://github.com/knative-sandbox/kn-plugin-quickstart/releases/download/knative-v${KNATIVE_VERSION}/kn-quickstart-linux-amd64"

            chmod +x ~/kn-quickstart

            sudo mv -v ~/kn-quickstart /usr/local/bin
            kn quickstart version
        else
            echo "Already present "
            kn version
            kn-quickstart version
        fi

        echo -e "\n${WAVE} Knative - func "

        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v func >/dev/null ||
            ! command -v kn-func >/dev/null; then
            #func cli
            curl -L -o ~/func "https://github.com/knative/func/releases/download/knative-v${FUNC_VERSION}/func_linux_amd64"

            chmod +x ~/func

            sudo mv -v ~/func /usr/local/bin
            func version

            #create symbolic link
            if ! command -v kn-func; then
                sudo ln -s /usr/local/bin/func /usr/local/bin/kn-func
            fi
        else
            echo "Already present "
            func version
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_knative "$@"

#!/bin/bash
function linuxb_k3d() {
    echo -e "\n${BUFFALO} Kubernetes - k3d "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install kubernetes - k3d "
        echo "https://k3d.io/v5.4.7/"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            ${LINUXB_PKG_INSTALL} k3d
            return 0
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v k3d >/dev/null; then
            # Before_script
            # curl
            if ! command -v curl >/dev/null; then
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} curl
            fi
            # Script
            curl -s https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | bash
        else
            echo "Already present "
            k3d --version
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_k3d "$@"

#!/bin/bash
function linuxb_helm() {
    echo -e "\n${DRAGON} Helm "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Helm "
        echo "https://helm.sh/fr/docs/intro/install/"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            ${LINUXB_PKG_INSTALL} helm
            return 0
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v helm >/dev/null; then
            # Before_Script
            # curl
            if ! command -v curl >/dev/null; then
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} curl
            fi
            # Script
            curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
        else
            echo "Already present "
            helm version
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_helm "$@"

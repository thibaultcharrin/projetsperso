#!/bin/bash
function linuxb_docker() {
	echo -e "\n${WHALE} Docker "

	case $1 in
	"--help" | "help" | "-h" | "h")
		echo "Install Docker "
		echo "https://docs.docker.com/engine/install/debian/"
		echo "https://docs.k3s.io/advanced#additional-preparation-for-debian-buster-based-distributions"
		;;
	*)
		linuxb_os_check

		# MACOS
		if [[ ${LINUXB_OS} == "Darwin" ]]; then
			echo "macOS is not yet supported"
			return 1
		fi

		# LINUXB
		if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
			! command -v docker >/dev/null; then
			case $(grep "^ID=" /etc/os-release | cut -d "=" -f 2 | tr -d '"') in
			"debian")
				# Before_Script
				# ca-certificates curl
				if [[ ! -d /usr/share/ca-certificates/ ]] ||
					! command -v curl >/dev/null; then
					${LINUXB_PKG_UPDATE}
					${LINUXB_PKG_INSTALL} ca-certificates curl
				fi
				# Script
				sudo install -m 0755 -d /etc/apt/keyrings
				sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
				sudo chmod a+r /etc/apt/keyrings/docker.asc

				echo \
					"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  					$(. /etc/os-release && echo "$VERSION_CODENAME") stable" |
					sudo tee /etc/apt/sources.list.d/docker.list >/dev/null
				${LINUXB_PKG_UPDATE}

				${LINUXB_PKG_INSTALL} docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

				#known issue with iptables
				sudo iptables -F
				sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
				sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy

				#add root privileges
				if [[ "$(getent group docker | grep -c "${USER}")" == 0 ]]; then
					getent group docker || sudo groupadd docker
					sudo usermod -aG docker "${USER}"
				fi
				;;
			"ubuntu")
				# Before_Script
				# ca-certificates curl
				if [[ ! -d /usr/share/ca-certificates/ ]] ||
					! command -v curl >/dev/null; then
					${LINUXB_PKG_UPDATE}
					${LINUXB_PKG_INSTALL} ca-certificates curl
				fi
				# Script
				sudo install -m 0755 -d /etc/apt/keyrings
				sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
				sudo chmod a+r /etc/apt/keyrings/docker.gpg

				echo \
					"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  					$(. /etc/os-release && echo "$VERSION_CODENAME") stable" |
					sudo tee /etc/apt/sources.list.d/docker.list >/dev/null
				${LINUXB_PKG_UPDATE}

				${LINUXB_PKG_INSTALL} docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

				#Known issue with iptables
				sudo iptables -F
				sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
				sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy

				#add root privileges
				if [[ "$(getent group docker | grep -c "${USER}")" == 0 ]]; then
					getent group docker || sudo groupadd docker
					sudo usermod -aG docker "${USER}"
				fi
				;;
			"ol")
				${LINUXB_PKG_INSTALL} dnf-plugins-core
				sudo dnf config-manager --add-repo https://download.docker.com/linux/rhel/docker-ce.repo
				${LINUXB_PKG_INSTALL} docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

				#known issue with iptables
				sudo iptables -F
				sudo update-alternatives --set iptables /usr/sbin/iptables-legacy

				#add root privileges
				if [[ "$(getent group docker | grep -c "${USER}")" == 0 ]]; then
					getent group docker || sudo groupadd docker
					sudo usermod -aG docker "${USER}"
				fi

				# enable daemon
				sudo systemctl enable --now docker
				sudo systemctl enable --now containerd
				;;
			"fedora")
				${LINUXB_PKG_INSTALL} dnf-plugins-core
				sudo dnf-3 config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
				${LINUXB_PKG_INSTALL} docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

				#known issue with iptables
				sudo iptables -F
				sudo update-alternatives --set iptables /usr/sbin/iptables-legacy

				#add root privileges
				if [[ "$(getent group docker | grep -c "${USER}")" == 0 ]]; then
					getent group docker || sudo groupadd docker
					sudo usermod -aG docker "${USER}"
				fi

				# enable daemon
				sudo systemctl enable --now docker
				sudo systemctl enable --now containerd
				;;
			"opensuse-tumbleweed")
				${LINUXB_PKG_INSTALL} docker docker-compose docker-compose-switch

				#add root privileges
				if [[ "$(getent group docker | grep -c "${USER}")" == 0 ]]; then
					getent group docker || sudo groupadd docker
					sudo usermod -aG docker "${USER}"
				fi

				# enable daemon
				sudo systemctl enable --now docker
				sudo systemctl enable --now containerd
				;;
			"arch")
				${LINUXB_PKG_UPDATE}
				${LINUXB_PKG_INSTALL} crun docker docker-compose

				#add root privileges
				if [[ "$(getent group docker | grep -c "${USER}")" == 0 ]]; then
					getent group docker || sudo groupadd docker
					sudo usermod -aG docker "${USER}"
				fi

				# enable daemon
				sudo systemctl enable --now docker
				sudo systemctl enable --now containerd
				;;
			*)
				echo "Generic installation not yet supported"
				return 1
				;;
			esac
		else
			echo "Already present "
			docker --version
		fi
		return 0
		;;
	esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_docker "$@"

<div align="center">
    <img src="logo.png" alt="Linux Logo" width="256" />
    <h1>Linuxb CLI - Bootstrap your Linux Distribution</h1>
</div>

A Shell-based project to bootstrap your Linux distribution with a selection of packages for Development, DevOps and System Administration.

Our project's main goal is to offer a CLI built from a variety of documented convenience scripts, simplifying the replication process of virtual development environments.  

<div align="center">
    <img src="screen.png" width="824" alt="Screenshot"/>
</div>

# Prerequisites

## System Requirements

- Linux (x86_64), Darwin (x86_64)

- Bash (GNU bash 5+)

- [Git](https://git-scm.com/downloads) (git-scm 2+)

This project is regularly being tested on a variety of Linux distributions and UNIX-like systems. Recently on:

- **Debian GNU/Linux 12 (bookworm)**
- **Ubuntu 24.04.2 LTS**
- **Fedora Linux 41**
- **openSUSE Tumbleweed**
- **Arch Linux**
- **Oracle Linux Server 9.5**

**NB**: Using different Linux distributions and/or different versions of the above packages might affect results/performance. If using macOS, you may need to ['linuxify'](https://github.com/darksonic37/linuxify) your host system and install [Homebrew](https://brew.sh) prior to using linuxb-cli. 

## Recommended 

Prefer a **new clean instance** of a **virtual machine** to avoid unwanted alterations to your host system configuration. 

We use **WSL2 (Windows Subsystem for Linux)**. Find out how to get started [**WSL Documentation**](https://docs.microsoft.com/en-us/windows/wsl/). 

Once your Linux distribution is ready, Open your favorite Linux Terminal. On [**Windows Terminal**](https://apps.microsoft.com/store/detail/windows-terminal/9N0DX20HK701?hl=en-us&gl=en), select your distribution from the dropdown list when opening a new tab.

# Getting Started

Install **latest** version:

```bash
git clone https://gitlab.com/devops-collab/linuxb.git ~/linuxb
cd ~/linuxb/
./install.sh
```

Install specific version:

- **version >= 1.4.0**:

```bash
# for example, version 1.10.0:
export VERSION=1.10.0
git clone -b 1.10.0 --depth=1 https://gitlab.com/devops-collab/linuxb.git ~/linuxb
cd ~/linuxb/
./install.sh
```

- **version < 1.4.0**:

```bash
# for example, version 1.3.0:
export VERSION=1.3.0
git clone -b 1.3.0 --depth=1 https://gitlab.com/devops-collab/linuxb.git ~/linuxb
cd ~/linuxb/
./build.sh
```

Two executable scripts respectively called `linuxb` and `linuxb-uninstall` are built and copied into `/usr/local/bin`.

# Running _Linux CLI_

Launch `linuxb` to display information about the available commands and options:

```bash
linuxb
```

**NB**: Passing `--help` (or `-h` for shorthand) as option or mistyping any of the commands displays the same information. 

```bash
linuxb all        # install all packages
```
```bash
linuxb install [PACKAGE]   # install specific package
```
```bash
linuxb list       # list all available packages
```
```bash
linuxb upgrade    # upgrade all packages
```
```bash
linuxb --help       # display this help and exit, shorthand "-h"
```
```bash
linuxb --version    # output version information and exit, shorthand "-v"
```

**NB**: You may chose to execute a script or a selection of scripts manually instead. 


## _Optional Steps_:

### **Handling Daemons in WSL2**

WSL2 previously lacked support for daemons implying that a number of background services would need to be triggered manually. This issue has now been addressed with the latest release.  

Run the following:

```bash
linuxb wsl  # enable systemd for wsl2
```

If your WSL needs updating, follow instructions here: [Systemd support](https://devblogs.microsoft.com/commandline/systemd-support-is-now-available-in-wsl/).

### **Configuring Git SCM**

**.gitconfig**

Configure your git username:

```bash
git config --global user.name #username
```

Configure your git email:

```bash
git config --global user.email #email
```

Configure pull strategy (default: merge):

```bash
git config --global pull.rebase false
```

**GitLab**

Create a **SSH Key Pair** and copy it to your **GitLab preferences**:

```bash
mkdir ~/.ssh && ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519 && cat ~/.ssh/id_ed25519.pub
```

# Bonus _[Oh My Zsh](https://github.com/ohmyzsh/ohmyzsh)_

Make your Linux Shell look incredible thanks to the wonderful Oh My Zsh Community-driven framework for ZShell. 

Run the following command to install Zsh and Oh My Zsh framework:

```bash
linuxb zsh  # shorthand 'z'
```

You may customize your shell running the `omz` cli. 

**NB**: Some previously installed packages use environment variables which might have been set to `~/.bashrc`. In order for them to function correctly, force install those packages and inspect `~/.zshrc`.

Run the following command to install p10k theme:

```bash
linuxb install p10k     # shorthand 'i p10k'
```

You may download fonts, install them from `~/fonts` and set your terminal to use `MesloLGS NF`. Customize your theme by running the `p10k configure` cli.

Additionally, you may install zsh-autosuggestions and zsh-syntax-highlighting plugins: 

```bash
linuxb install plugins  # shorthand 'i plugins'
```


## You are all set!

Enjoy your new Linux environment. Spread the word, give this project a star, and feel free to reach out for suggestions.

# Credits

- [Thibault Charrin](https://gitlab.com/thibaultcharrin) (_DevOps | Full Stack Developper_)
    
    - Main author, script implementation

- [Patrick Chausson](https://gitlab.com/patrick.c.dog1) (_DevOps_)

    - Contributor

- [alivat0r](https://macosicons.com/#/u/alivat0r)

    - Logo

- [darksonic37](https://github.com/darksonic37)

    - linuxify (for macOS)

- [Homebrew](https://brew.sh)

    - The Missing Package Manager for macOS (or Linux)

- [Craig Loewen](https://devblogs.microsoft.com/commandline/systemd-support-is-now-available-in-wsl/)

    - Systemd support for WSL

- [Alexandr Marynkin](https://unsplash.com/@mrnknag)

    - Screen background

- [Oh My Zsh](https://github.com/ohmyzsh/ohmyzsh)

    - Community-driven framework for ZShell

- [Romaktv](https://github.com/romkatv)

    - Powerlevel10k Theme for Oh My Zsh

- [zsh-users](https://github.com/zsh-users)

    - Autosuggestions and Syntax-Highlighting for Oh My Zsh
    